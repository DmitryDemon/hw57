import React, { Component } from 'react';
import './App.css';
import AddTaskForm from './Companents/ToDo/AddTaskForm';
import Task from "./Companents/ToDo/Task";

class App extends Component {

    state = {
        tasks: [],
        textInput: '',
        costInput: 0,
        totalCount: 0,
    };

    addTask = () => {
        if (this.state.textInput !== '') {
            const tasks =  [...this.state.tasks];
            const newTask = {
                text: this.state.textInput,
                cost: this.state.costInput,

                id: Date.now()
            };

            tasks.push(newTask);

            let totalPrice = tasks.reduce((total, item) => total + parseInt(item.cost), this.state.totalCount);

            this.setState({tasks,totalPrice});
        }
    };


    removeItems = id => {
        const tasks = [...this.state.tasks];

        const index = tasks.findIndex(task => {
            return (
                task.id === id
            )
        });
        tasks.splice(index, 1);
        let totalPrice = tasks.reduce((total, item) => total + parseInt(item.cost), this.state.totalCount);
        this.setState({tasks, totalPrice})
    };

    changeHandler = (event) => {
        this.setState({
            textInput: event.target.value,
        })
    };
    changeHandlerCost = (event) => {
        this.setState({
            costInput: event.target.value,
        })
    };

    render() {
        return (
            <div className="App">
                <AddTaskForm cost={(event)=> this.changeHandlerCost(event)} change={(event) => this.changeHandler(event)} add={() => this.addTask()}/>
                <Task entries = {this.state.tasks}
                      remove = {(id) => this.removeItems(id)}
                />
                <h2 className='total'>Total spend:{this.state.totalPrice}</h2>
            </div>
        );
    }
}

export default App;
