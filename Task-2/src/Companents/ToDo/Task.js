import React from 'react';
import './ToDo.css';

const Task = (props) =>  {

        return (
            props.entries.map((task) => {
                return (
                    <div>
                        <div className='task' key={task.id}>
                            <p>{task.text}</p>
                            <p className='cost'>{task.cost}KGS</p>

                            <button onClick={() => props.remove(task.id)} className='btn-remove'>Remove</button>
                        </div>
                    </div>
                )
            })
        )

};

export default Task;