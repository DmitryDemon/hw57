
const tasks = [
    {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
    {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
    {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
    {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
    {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
    {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
    {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];

const totalCountFrontend = tasks.reduce((previousValue,item) => {
    if(item.category === 'Frontend'){
        previousValue += item.timeSpent
    }
    return previousValue;

}, 0);

const totalCountBug = tasks.reduce((previousValue,item) => {
    if(item.type === 'bug'){
        previousValue += item.timeSpent
    }
    return previousValue;

}, 0);

const countValueUiInArr = tasks.filter(item => item.title.includes('UI')).length;

const frequenciesTaskObgect = tasks.reduce((acc , item)=>{
    const categ = item.category;
    if (categ in acc) {
        acc[categ]++;
    }else acc[categ] = 1;
    return acc;
},{});

const arrayOfObjects = tasks.map(item => {
    if (item.timeSpent > 4) return {title: item.title, category: item.category}
} ).filter(item => item !== undefined);





console.log('Общее колличество времени затраченное на задачу "Frontend"',totalCountFrontend,'часов');
console.log('Общее колличество времени затраченное на задачу типа "bug"',totalCountBug,'часов');
console.log('Колличество значений "UI" встречается в массиве ',countValueUiInArr,'раза');
console.log(frequenciesTaskObgect);
console.log(arrayOfObjects);


